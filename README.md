# PrivacyPresets

My personal configs for privacy add-ons and software.

----

## Index:

* [Arkenfox](https://codeberg.org/ADHDefy/PrivacyPresets/src/branch/main/README.md#arkenfox-user-overrides)
* [Redirector](https://codeberg.org/ADHDefy/PrivacyPresets#redirector)
* [Skip Redirect](https://codeberg.org/ADHDefy/PrivacyPresets/src/branch/main/README.md#skip-redirect)
* [uBlock Origin](https://codeberg.org/ADHDefy/PrivacyPresets#ublock-origin)

## How to use these

### Arkenfox User-Overrides

These are my personal overrides for the Arkenfox user.js--I won't go into detail here about how to use this, but I highly encourage you to check out [their wiki](https://github.com/arkenfox/user.js/wiki) for detailed setup instructions.

### Redirector

Redirector is a cool add-on that will automatically redirect anything to anything else. For me personally, I use it to automatically access links from creepy-tracky sites (technical nomenclature) on a more privacy-friendly alternative frontend. This is a bunch of presets that I personally use. Feel free to check it out.

1. Install [Redirector](https://addons.mozilla.org/en-US/firefox/addon/redirector/)
2. Download [Redirector_custom-redirects.json](https://codeberg.org/ADHDefy/PrivacyPresets/raw/branch/main/Redirector/Redirector_custom-redirects.json) (Right-click > "Save Page As...")
3. Click the Redirector icon in your browser and select "Edit Redirects" in the dropdown
4. In the new tab that opens, delete the example redirect
5. Select "Import"
6. Navigate to the file downloaded in Step 2 and select it
7. Scroll through the imported redirects and enable/disable them by your personal preferences

**Note:** *If you choose to use the "Mastodon Easy Remote Follow" redirect, make sure to edit it for your specific instance!*

Boop. We done.

**Included Redirects:**

>* AMP > HTML
>* Mobile Site > Desktop Site
>* Doubleclick Escaper
>* Fandom > BreezeWiki
>* GitHub > GotHub
>* Google Maps > DuckDuckGo (Apple Maps)
>* Google Maps > OpenStreetMaps (*Disabled by default*)
>* Google Seach > Startpage Search
>* Google Translate > Lingva Translate
>* IMDB > libremd (*Broken*)
>* Imgur > Imgin
>* Instagram > Bibliogram (*Broken*)
>* Mastodon Easy Remote Follows (*Disabled by default*)
>* Medium > Scribe
>* Odyssey > Librarian
>* Quora > Quetre
>* Reddit > Libreddit (*Disabled by default*)
>* Reddit Redesign > Old Reddit (*Disabled by default*)
>* Reuters > Neuters
>* StackOverflow > AnonymousOverflow
>* TikTok > ProxiTok
>* Twitter > Nitter
>* Wikipedia > Wikiless
>* YouTube > Invidious
>* YouTube > Piped (*Disabled by default*)

### Skip Redirect

Unfortunately, there's no "easy one-file import" for this one, as there's currently no way to import/export lists from this add-on. That said, it's still pretty simple to do. 

1. Install [Skip Redirect](https://addons.mozilla.org/en-US/firefox/addon/skip-redirect/)
2. Navigate to the [No-skip-urls-list.txt](https://codeberg.org/ADHDefy/PrivacyPresets/raw/branch/main/Skip%20Redirect/No-skip-urls-list.txt) file
3. Highlight only the URLs from the catergories you need and Copy them (CTRL+C, or right-click > "Copy")
4. Click on the Skip Redirect icon to bring down the drop-down menu
5. Under the text box labeled, "No-skip-urls-list (you may use regular expressions)," scroll down to the bottom of the list
6. Create a new line under "/verification" and Paste (CTRL+V, right-click > "Paste" in the URL list you copied in step 3)

**Note:** *If you use PeerTube, I recommend adding your instance's domain to this list, as well, as it will make remote subscribing easier. A list of common instances is provided, but you may need to add yours separately.*

Pew pew. Ya did it.

These redirects are ones that I've come across in recent years, and will be especially helpful if you use the [Reverse Image Search](https://addons.mozilla.org/en-US/firefox/addon/image-reverse-search/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search) add-on. 

### uBlock Origin

These settings are configured to put uBlock Origin in [Easy Mode](https://github.com/gorhill/uBlock/wiki/Blocking-mode) with several additional blocklists to expand its functionality. It will filter URL tracking bits and be more efficient at weeding out other web hazards besides ads, such as known phishing servers, coin miners, spam, etc. This setup should be pretty much set-and-forget, and great for beginners.

Power users can crank up the filter mode and try out the dynamic filtering. I am not comfortable sharing my own dynamic filters, since it would reveal a bunch about my personal browsing habits. That said, this setup should get you rolling.

1. Install [uBlock Origin](https://ublockorigin.com/) for your browser
2. Download [uBlock-Origin_blocklists.txt](https://codeberg.org/ADHDefy/PrivacyPresets/raw/branch/main/uBlock%20Origin/uBlock-Origin_blocklists.txt) (Right-click > "Save Page As...")
3. In uBlock Origin, click the cog icon to open the Dashboard
4. On the "Settings" tab of the Dashboard, scroll down to the bottom and find the "Restore from file..." button
5. Navigate to the file you downloaded in Step 2 and select it
6. Change from the "Settings" tab at the top to "Filter Lists"
7. (Optional) Feel free to disable any blocklists that you don't want by unchecking them
8. Click "Apply" and then "Update now" (if not greyed out)

Ta-da! You're done. Enjoy a more private, ad-free browsing experience.

**Included Lists:**

>* [Actually Legitimate URL Shortener](https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt) (***Essential***)
>* [ClearURLs for uBO](https://raw.githubusercontent.com/DandelionSprout/adfilt/master/ClearURLs%20for%20uBo/clear_urls_uboified.txt) (***Essential***)
>* [AdGuard Tracking Protection]() (*Recommended*)
>* [Phishing Army Extended](https://phishing.army/download/phishing_army_blocklist_extended.txt) (*Recommended*)
>* [NoCoin](https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/nocoin.txt) (*Recommended*)
>* [Spam404](https://raw.githubusercontent.com/Spam404/lists/master/adblock-list.txt) (*Recommended*)
>* [Durablenapkin's Scam Blocklist](https://raw.githubusercontent.com/durablenapkin/scamblocklist/master/hosts.txt) (*Recommended*)
>* [GoodbyeAds Adblock Filter](https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Formats/GoodbyeAds-AdBlock-Filter.txt) (*Optional*)
>* [GoodbyeAds YouTube AdBlock Filter](https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Formats/GoodbyeAds-YouTube-AdBlock-Filter.txt) (*Optional*)
>* [Lightswitch05's Ads and Tracking Extended](https://www.github.developerdan.com/hosts/lists/ads-and-tracking-extended.txt) (*Optional*)
>* [Lightswitch05's AMP Hosts Extended](https://www.github.developerdan.com/hosts/lists/amp-hosts-extended.txt) (*Optional*)
>* [Lightswitch05's Hate and Junk Extended](https://www.github.developerdan.com/hosts/lists/hate-and-junk-extended.txt) (*Optional*)
>* [Web Annoyances Ultralist](https://raw.githubusercontent.com/yourduskquibbles/webannoyances/master/ultralist.txt) (*Optional*)
>* [Bypass Paywalls Clean](https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/bpc-paywall-filter.txt) (*Optional*)
>* [FMHY Unsafe Sites/Software](https://gist.githubusercontent.com/Rust1667/df78d493cf3c00340c535d93e303c4f9/raw) (*Optional*)
>* [Adblock The NSA](https://raw.githubusercontent.com/gasull/adblock-nsa/master/filters.txt) (*Optional*)
>* [Anti Fake News International](https://raw.githubusercontent.com/AFNIL-AntiFakeNewsList/AFNIL/master/AFNIL-ABP-uBO.txt) (*Optional*)
>* [InfinitySec's Medical Pseudoscience](https://raw.githubusercontent.com/infinitytec/blocklists/master/medicalpseudoscience.txt) (*Optional*)

----

Thanks for reading! There is most certainly more to come.