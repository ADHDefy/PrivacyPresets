
/*** MY OVERRIDES ***/


/*** [SECTION 0100]: STARTUP ***/

/* 0102: set startup page [SETUP-CHROME]
 * 0=blank, 1=home, 2=last visited page, 3=resume previous session
 * [NOTE] Session Restore is cleared with history (2811, 2812), and not used in Private Browsing mode
 * [SETTING] General>Startup>Restore previous session ***/
user_pref("browser.startup.page", 1);
user_pref("browser.startup.homepage", "https://startpage.com");


/*** [SECTION 0400]: SAFE BROWSING (SB) ***/

// 0401: disable SB (Safe Browsing)
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);

// 0402: disable SB checks for downloads (both local lookups + remote)
user_pref("browser.safebrowsing.downloads.enabled", false);

// 0403: disable SB checks for downloads (remote)
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.url", "");

// 0404: disable SB checks for unwanted software
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);

// 0405: disable "ignore this warning" on SB warnings [FF45+]
user_pref("browser.safebrowsing.allowOverride", false);


/*** [SECTION 0700]: DNS / DoH / PROXY / SOCKS / IPv6 ***/

/* 0710: disable DNS-over-HTTPS (DoH) rollout [FF60+]
 * 0=off by default, 2=TRR (Trusted Recursive Resolver) first, 3=TRR only, 5=explicitly off
 * see "doh-rollout.home-region": USA 2019, Canada 2021, Russia/Ukraine 2022 [3]
 * [1] https://hacks.mozilla.org/2018/05/a-cartoon-intro-to-dns-over-https/
 * [2] https://wiki.mozilla.org/Security/DOH-resolver-policy
 * [3] https://support.mozilla.org/en-US/kb/firefox-dns-over-https
 * [4] https://www.eff.org/deeplinks/2020/12/dns-doh-and-odoh-oh-my-year-review-2020 ***/
user_pref("network.trr.mode", 3);


/*** [SECTION 1200]: HTTPS (SSL/TLS / OCSP / CERTS / HPKP) ***/

// 1246: disable HTTP background requests [FF82+]
user_pref("dom.security.https_only_mode_send_http_background_request", false);


/*** [SECTION 1600]: HEADERS / REFERERS

/* 1601: control when to send a cross-origin referer
 * 0=always (default), 1=only if base domains match, 2=only if hosts match
 * [SETUP-WEB] Breakage: older modems/routers and some sites e.g banks, vimeo, icloud, instagram
 * If "2" is too strict, then override to "0" and use Smart Referer extension (Strict mode + add exceptions) ***/
user_pref("network.http.referer.XOriginPolicy", 0);


/*** [SECTION 1700]: CONTAINERS

/* 1701: enable Container Tabs and its UI setting [FF50+]
 * [SETTING] General>Tabs>Enable Container Tabs ***/
user_pref("privacy.userContext.enabled", false);
user_pref("privacy.userContext.ui.enabled", false);


/*** [SECTION 2000]: PLUGINS / MEDIA / WEBRTC ***/

// 2020: enable GMP (Gecko Media Plugins)
// user_pref("media.gmp-provider.enabled", true);

/* 2022: enable all DRM content (EME: Encryption Media Extension)
 * Optionally hide the setting which also disables the DRM prompt
 * [SETUP-WEB] e.g. Netflix, Amazon Prime, Hulu, HBO, Disney+, Showtime, Starz, DirectTV
 * [SETTING] General>DRM Content>Play DRM-controlled content
 * [TEST] https://bitmovin.com/demos/drm ***/
// user_pref("media.eme.enabled", true);
// user_pref("browser.eme.ui.enabled", false);


/*** [SECTION 2400]: DOM (DOCUMENT OBJECT MODEL) ***/

// 2403: block popup windows
// [SETTING] Privacy & Security>Permissions>Block pop-up windows
user_pref("dom.disable_open_during_load", false);


/*** [SECTION 2800]: SHUTDOWN & SANITIZING ***/

/** SANITIZE ON SHUTDOWN: RESPECTS "ALLOW" SITE EXCEPTIONS FF103+ ***/
/* 2815: set "Cookies" and "Site Data" to clear on shutdown (if 2810 is true) [SETUP-CHROME]
 * [NOTE] Exceptions: A "cookie" block permission also controls "offlineApps" (see note below).
 * serviceWorkers require an "Allow" permission. For cross-domain logins, add exceptions for
 * both sites e.g. https://www.youtube.com (site) + https://accounts.google.com (single sign on)
 * [NOTE] "offlineApps": Offline Website Data: localStorage, service worker cache, QuotaManager (IndexedDB, asm-cache)
 * [WARNING] Be selective with what sites you "Allow", as they also disable partitioning (1767271)
 * [SETTING] to add site exceptions: Ctrl+I>Permissions>Cookies>Allow (when on the website in question)
 * [SETTING] to manage site exceptions: Options>Privacy & Security>Permissions>Settings ***/
// user_pref("privacy.clearOnShutdown.cookies", false); // Cookies
// user_pref("privacy.clearOnShutdown.offlineApps", false); // Site Data


/*** [SECTION 4500]: RFP (RESIST FINGERPRINTING) ***/

/* 4501: enable privacy.resistFingerprinting [FF41+]
 * [SETUP-WEB] RFP can cause some website breakage: mainly canvas, use a site exception via the urlbar
 * RFP also has a few side effects: mainly timezone is UTC0, and websites will prefer light theme
 * [1] https://bugzilla.mozilla.org/418986 ***/
user_pref("privacy.resistFingerprinting", true);
// 4504: enable RFP letterboxing [FF67+]
user_pref("privacy.resistFingerprinting.letterboxing", true); // [HIDDEN PREF]


/*** [SECTION 5000]: OPTIONAL OPSEC ***/

// 5001: start Firefox in PB (Private Browsing) mode
// [SETTING] Privacy & Security>Logins and Passwords>Ask to save logins and passwords for websites
user_pref("signon.rememberSignons", false);

// 5010: disable location bar suggestion types
// [SETTING] Privacy & Security>Address Bar>When using the address bar, suggest ***/
user_pref("browser.urlbar.suggest.history", false);
user_pref("browser.urlbar.suggest.bookmark", false);
user_pref("browser.urlbar.suggest.openpage", false);
user_pref("browser.urlbar.suggest.topsites", false); // [FF78+]


/*** [SECTION 9000]: PERSONAL ***/

/*** UX FEATURES ***/
user_pref("extensions.pocket.enabled", false); // Pocket Account [FF46+]
//user_pref("extensions.screenshots.disabled", true); // [FF55+]


/*** CUSTOM ***/

// DoH Custom URI
user_pref("network.trr.uri", https://doh.mullvad.net/dns-query);
user_pref("network.trr.custom_uri", https://doh.mullvad.net/dns-query);

// Show Search widget in navbar
user_pref("browser.search.widget.inNavBar", true);                         

// Disallow websites to see battery status
user_pref("dom.battery.enabled", false);    

// Stop add-on update redirects
user_pref("browser.tabs.loadDivertedInBackground", true);
